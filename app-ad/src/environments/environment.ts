// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyBONBeyb0fCBm981nJSLOsz72oM5o1x2m0",
  authDomain: "artisandraw.firebaseapp.com",
  databaseURL: "https://artisandraw.firebaseio.com",
  projectId: "artisandraw",
  storageBucket: "artisandraw.appspot.com",
  messagingSenderId: "1052426332346",
  appId: "1:1052426332346:web:667bef027ad905263cba83",
  measurementId: "G-HSQ578FSK5"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
