import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from "./guards/auth/auth.guard"; // Si alguien no ha iniciado sesión, se manda al login
import { LoggedGuard } from "./guards/logged/logged.guard"; // Si ha iniciado sesión, se manda a home


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canActivate : [LoggedGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule),
    canActivate : [LoggedGuard]
  },
  {
    path: 'profile/:id',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'profile/edit',
    loadChildren: () => import('./pages/profile/edit/edit.module').then( m => m.EditPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'publications/add-publication',
    loadChildren: () => import('./pages/publications/add-publication/add-publication.module').then( m => m.AddPublicationPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'publications/edit-publication',
    loadChildren: () => import('./pages/publications/edit-publication/edit-publication.module').then( m => m.EditPublicationPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'publications/:id',
    loadChildren: () => import('./pages/publications/publications.module').then( m => m.PublicationsPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'publications/publication/:id',
    loadChildren: () => import('./pages/publications/publication/publication.module').then( m => m.PublicationPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'shop/:id',
    loadChildren: () => import('./pages/shop/shop.module').then( m => m.ShopPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'products/add-product',
    loadChildren: () => import('./pages/products/add-product/add-product.module').then( m => m.AddProductPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'folders/:id',
    loadChildren: () => import('./pages/folders/folders.module').then( m => m.FoldersPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'folders/folder/:id',
    loadChildren: () => import('./pages/folders/folder/folder.module').then( m => m.FolderPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'product/:id',
    loadChildren: () => import('./pages/products/product/product.module').then( m => m.ProductPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'configuration',
    loadChildren: () => import('./pages/configuration/configuration.module').then( m => m.ConfigurationPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'profile/:id/configure-paypal/:code',
    loadChildren: () => import('./pages/profile/configure-paypal/configure-paypal.module').then( m => m.ConfigurePaypalPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'products/confirm-purchase/:id',
    loadChildren: () => import('./pages/products/confirm-purchase/confirm-purchase.module').then( m => m.ConfirmPurchasePageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'orders/:iduser',
    loadChildren: () => import('./pages/orders/orders.module').then( m => m.OrdersPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'commission/:id',
    loadChildren: () => import('./pages/commission/commission.module').then( m => m.CommissionPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'orders-user/:iduser',
    loadChildren: () => import('./pages/orders-user/orders-user.module').then( m => m.OrdersUserPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'orders/:iduser/order/:idorder',
    loadChildren: () => import('./pages/orders/order/order.module').then( m => m.OrderPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'pay-commission/:id',
    loadChildren: () => import('./pages/pay-commission/pay-commission.module').then( m => m.PayCommissionPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'messages',
    loadChildren: () => import('./pages/messages/messages.module').then( m => m.MessagesPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'send-message/:iduser',
    loadChildren: () => import('./pages/messages/send-message/send-message.module').then( m => m.SendMessagePageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'message/:id',
    loadChildren: () => import('./pages/messages/message/message.module').then( m => m.MessagePageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'reply/:id',
    loadChildren: () => import('./pages/messages/reply/reply.module').then( m => m.ReplyPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'configuration/edit-commission',
    loadChildren: () => import('./pages/configuration/edit-commission/edit-commission.module').then( m => m.EditCommissionPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'configuration/delete-account',
    loadChildren: () => import('./pages/configuration/delete-account/delete-account.module').then( m => m.DeleteAccountPageModule),
    canActivate : [AuthGuard]
  },
  {
    path: 'configuration/edit-pass',
    loadChildren: () => import('./pages/configuration/edit-pass/edit-pass.module').then( m => m.EditPassPageModule),
    canActivate : [AuthGuard]
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
