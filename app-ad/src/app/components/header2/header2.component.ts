import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.scss']
})
export class Header2Component implements OnInit {

	public isAuth : boolean;
	public pagActual : string = window.location.pathname;

  constructor(private menu: MenuController, private auth_fire: AngularFireAuth) {
	this.auth_fire.auth.onAuthStateChanged ( user => {
		if (user) {
			this.isAuth = true;
		} else {
			this.isAuth = false;
		}
	});
   }

  ngOnInit() {}

  activarMenu () {
	this.menu.toggle('menu');
  }

}
