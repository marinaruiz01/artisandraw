import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { AngularFireAuth } from '@angular/fire/auth';
import { PopoverController, MenuController } from '@ionic/angular';

@Component({
	selector: 'app-popover',
	templateUrl: './popover.component.html',
	styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

	public isAuth: boolean;

	constructor(private authService: AuthService, private auth_fire: AngularFireAuth, private popoverCtrl: PopoverController,
		private menuCtrl: MenuController) {
		this.auth_fire.auth.onAuthStateChanged(user => {
			if (user) {
				this.isAuth = true;
			} else {
				this.isAuth = false;
			}
		});
	}

	ngOnInit() { }

	signOut() {
		this.authService.logout();
		this.popoverCtrl.dismiss();
		this.menuCtrl.toggle();
		this.isAuth = false;
	}

}
