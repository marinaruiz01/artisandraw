import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

	public isAuth : boolean;
	public pagActual : string = window.location.pathname;

  constructor(private menu: MenuController, private auth_fire: AngularFireAuth) {
	this.auth_fire.auth.onAuthStateChanged ( user => {
		if (user) {
			this.isAuth = true;
		} else {
			this.isAuth = false;
		}
	});
   }

  ngOnInit() {}

  activarMenu () {
	this.menu.toggle('menu');
  }

}
