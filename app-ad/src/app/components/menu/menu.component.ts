import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../popover/popover.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

	public isAuth: boolean;
	public pagActual: string = window.location.pathname;
	public userActual: string;
	public iduser: string;

  constructor(private popoverCtrl: PopoverController, private auth_fire: AngularFireAuth,
		  private router: Router, private authSvc: AuthService) { 
	this.auth_fire.auth.onAuthStateChanged ( user => {
		if (user) {
			this.isAuth = true;
			this.iduser = this.authSvc.getCurrentID();
		} else {
			this.isAuth = false;
		}
	});
	this.userActual =  "me";
  }

  ngOnInit() {}

  irPerfil() {
	  this.router.navigate(['/profile']);
  }

  async showPopover(event: any) {
	const popover = await this.popoverCtrl.create({
		component: PopoverComponent,
		event: event,
		mode: 'ios'
	});

	await popover.present();
  }

}
