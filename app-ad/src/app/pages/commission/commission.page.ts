import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { CommissionsService } from 'src/app/services/commissions.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-commission',
  templateUrl: './commission.page.html',
  styleUrls: ['./commission.page.scss'],
})
export class CommissionPage implements OnInit {
	public idusuario: string;
	public user: any;
	public nombre: string;

	public tipo: string;
	public tam: string;
	public presupuesto: number;
	public descripcion: string;

	public useractual: any;
	public username: string;
	public idusuarioactual: string;

  constructor(private route: ActivatedRoute, private usersSvc: UsersService, private commSvc: CommissionsService,
		  private authSvc: AuthService) { 
	this.idusuario = this.route.snapshot.paramMap.get('id');
	this.idusuarioactual = this.authSvc.getCurrentID();
	this.usersSvc.getUser(this.idusuario).subscribe(user => {
		this.user = user;
		this.nombre = this.user.usuario;
	})
	this.usersSvc.getCurrentUser().subscribe(user => {
		this.useractual = user;
		this.username = this.useractual.usuario;
	})
  }

  ngOnInit() {
  }

  pedir () {
	this.commSvc.newCommission(this.tipo, this.tam, this.descripcion, this.presupuesto, 
		this.username, this.idusuarioactual, this.idusuario, this.nombre, 0, 0, 'N/A', 'Pendiente de aprobación');
  }

}
