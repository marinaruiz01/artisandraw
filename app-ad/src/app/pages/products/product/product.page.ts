import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { ProductI } from 'src/app/interfaces/product';
import { UsersService } from 'src/app/services/users.service';
import { UserI } from 'src/app/interfaces/user';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-product',
	templateUrl: './product.page.html',
	styleUrls: ['./product.page.scss'],
})

export class ProductPage implements OnInit {

	public idproduct: string;
	public id_user: string;
	public product: any;
	public user: any;
	public usuario: string;
	public description: string;
	public price: number;
	public image: string;
	public title: string;
	public envio: number;

	public addScript: boolean = false;
	public producto: any;
	public cardDetails: any = {};
	public iduser: string;
	public idactual: string;

	constructor(private productSvc: ProductsService, private usersSvc: UsersService,
		private route: ActivatedRoute, private authSvc: AuthService) {
		this.idproduct = this.route.snapshot.paramMap.get('id');
		this.idactual = this.authSvc.getCurrentID();
		this.productSvc.getProduct(this.idproduct).subscribe(producto => {
			this.product = producto as ProductI;
			this.id_user = this.product.idusuario;
			this.price = this.product.precio;
			this.description = this.product.descripcion;
			this.title = this.product.titulo;
			this.image = this.product.imagen;
			this.envio = this.product.gastos;
			this.usersSvc.getUser(this.id_user).subscribe(usuario => {
				console.log("hola");
				this.user = usuario as UserI;
				this.iduser = this.user.id;
				this.usuario = this.user.usuario;
			});

		})

	}

	ngOnInit() {

	}

	borrar () {
		if (confirm ("¿Estás seguro de que quieres borrar el producto?")) {
			this.productSvc.deleteProduct(this.idproduct);
		}
	}
}
