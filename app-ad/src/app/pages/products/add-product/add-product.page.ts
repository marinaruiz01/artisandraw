import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ProductsService } from 'src/app/services/products.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
	selector: 'app-add-product',
	templateUrl: './add-product.page.html',
	styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {

	public usuarioActual: string;
	public numAleatorio: number;
	public refImage: string;
	public urlImage: string;
	
	public titulo: string;
	public descripcion: string;
	public precio: number;
	public gastos: number;
	public stock: number;
	public archivo: File;

	public user: any;
	public verificada: boolean;

	constructor(private authService: AuthService, private productsService: ProductsService, private usersSvc: UsersService) { 
		this.usersSvc.getCurrentUser().subscribe( usuario => {
			this.user = usuario;
			this.verificada = this.user.verificada;
		})
	}

	ngOnInit() {
	}

	setFile(event) {
		var file = event.target.files;
		this.archivo = file[0];
	}

	async subirProducto() {
		this.usuarioActual = this.authService.getCurrentID();
		this.numAleatorio= Math.round(Math.random()*1000000000);
		this.refImage = this.titulo + this.usuarioActual + this.numAleatorio;
		await this.productsService.uploadImage(this.refImage, this.archivo).then( res => {
			this.productsService.downloadImage(this.refImage).then (url => {
				this.urlImage = url;
				this.productsService.addProduct (this.titulo, this.descripcion, this.precio, this.gastos, this.stock,
									   this.urlImage, this.usuarioActual, this.refImage);
			}).catch ( err => console.log ("Error al leer la url de la imagen" + err));
		});
	}

}
