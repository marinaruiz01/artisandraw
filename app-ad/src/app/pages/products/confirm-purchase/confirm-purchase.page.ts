import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { ProductI } from 'src/app/interfaces/product';
import { UserI } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';
import { CommissionsService } from 'src/app/services/commissions.service';

declare var paypal;

@Component({
	selector: 'app-confirm-purchase',
	templateUrl: './confirm-purchase.page.html',
	styleUrls: ['./confirm-purchase.page.scss'],
})
export class ConfirmPurchasePage implements OnInit {
	@ViewChild('paypal', { static: true }) paypalElement: ElementRef;

	public idproduct: string;
	public id_user: string;
	public product: any;
	public user: any;
	public price: number;
	public envio: number;

	public addScript: boolean = false;
	public producto: any;
	public paypalConfig: any;

	public monedero: number;
	public iduser: string;
	public direccion: string;
	public nombre: string;
	public descripcion: string;
	public autor: string;

	constructor(private productSvc: ProductsService, private route: ActivatedRoute, private usersSvc: UsersService,
		private authSvc: AuthService, private commSvc: CommissionsService) {
		this.idproduct = this.route.snapshot.paramMap.get('id');
		this.productSvc.getProduct(this.idproduct).subscribe(producto => {
			this.product = producto as ProductI;
			this.id_user = this.product.idusuario;
			this.price = this.product.precio;
			this.envio = this.product.gastos;
			this.nombre = this.product.titulo;
			this.descripcion = this.product.descripcion;
			this.usersSvc.getUser(this.id_user).subscribe(usuario => {
				this.user = usuario as UserI;
				this.iduser = this.user.id;
				this.autor = this.user.usuario;
				this.monedero = this.user.monedero;
			});
			this.paypalConfig = {
				env: 'sandbox',
				client: {
					sandbox: 'AbqmTyzUauctATs-bFXvU70RZyHfbRePeOQyBkKcEJG03tsudZhGXyLha84k2AcuqfonBARHD3bWPT-3',
					production: ''
				},
				commit: true,
				payment: (data, actions) => {
					return actions.payment.create({
						payment: {
							transactions: [
								{
									amount: {
										total: this.price + this.envio,
										currency: 'EUR'
									}
								}
							]
						}
					});
				},
				onAuthorize: (data, actions) => {
					return actions.payment.execute().then(payment => {
						console.log(payment);
						this.monedero += this.price;
						let datos = {
							monedero: this.monedero
						}
						this.usersSvc.editUser(this.iduser, datos);
						var idactual = this.authSvc.getCurrentID();
						this.usersSvc.getCurrentUser().subscribe(user => {
							var useractual: any = user;
							var username = useractual.usuario;
							this.commSvc.newCommission('Tienda AD', this.nombre, this.descripcion, 0, username, idactual, this.id_user,
								this.autor, this.price, this.envio, this.direccion, 'Pagado');
						});
						console.log("Data: "+data);
						alert("Compra realizada correctamente. Puede ver el estado de su pedido en 'Pedidos realizados'.");
					})
				},
				onError: (data, actions) => {
					console.log ("data: "+data);
				}
			}

			if (!this.addScript) {
				this.addPaypalScript().then(() => {
					paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn')
				})
			}
			/*paypal.Button.render({
				env: 'sandbox',
				client: {
				    sandbox:    'AbqmTyzUauctATs-bFXvU70RZyHfbRePeOQyBkKcEJG03tsudZhGXyLha84k2AcuqfonBARHD3bWPT-3',
				    production: '<insert production client id>'
				},
				commit: true,
				payment: function(data, actions) {
				    return actions.payment.create({
					  payment: {
						transactions: [
						    {
							  amount: { total: '0.01', currency: 'EUR' }
						    }
						]
					  }
				    });
				},
				onAuthorize: function(data, actions) {
				    return actions.payment.execute().then(function() {
					  window.alert('Payment Complete!');
				    });
				}
			  }, '#paypal-checkout-btn');*/
		})
	}

	ngOnInit() {
	}


	addPaypalScript() {
		this.addScript = true;
		return new Promise((resolve, reject) => {
			let scriptTag = document.createElement('script');
			scriptTag.src = 'https://www.paypalobjects.com/api/checkout.js';
			scriptTag.onload = resolve;
			document.body.appendChild(scriptTag);
		})
	}

}
