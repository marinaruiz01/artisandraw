import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { PublicationsService } from 'src/app/services/publications.service';
import { AuthService } from 'src/app/services/auth.service';
import { ModalController } from '@ionic/angular';
import { ChooseCategoryPage } from '../../categories/choose-category/choose-category.page';
import { UsersService } from 'src/app/services/users.service';

@Component({
	selector: 'app-add-publication',
	templateUrl: './add-publication.page.html',
	styleUrls: ['./add-publication.page.scss'],
})
export class AddPublicationPage implements OnInit {

	public categories: any[];

	public user: any;
	public usernameActual: string;
	public archivo: File;
	public refImage: string;
	public titulo: string;
	public categoria: string;
	public urlImage: string;
	public usuarioActual: string;
	public numAleatorio: number;
	public listaCategorias: Array<string> = [];
	public mostrarCategorias: string = '';
	public listaCategoriasID: Array<string> = [];

	constructor(private categoriesService: CategoriesService, private publicationsService: PublicationsService,
			private authService: AuthService, private modalCtrl: ModalController, public userSvc: UsersService) { }

	ngOnInit() {
		this.categoriesService.getCategories().subscribe(categories => {
			this.categories = categories;
		})
	}

	setFile(event) {
		var file = event.target.files;
		this.archivo = file[0];
		return this.archivo;
	}

	async subirPublicacion() {
		this.usuarioActual = this.authService.getCurrentID();
		this.userSvc.getCurrentUser().subscribe( user => {
			this.user = user;
			this.usernameActual = this.user.usuario;
		})
		this.numAleatorio= Math.round(Math.random()*1000000000);
		this.refImage = this.titulo + this.usuarioActual + this.numAleatorio;
		await this.publicationsService.uploadImage(this.refImage, this.archivo).then( res => {
			console.log ("Va a descargarse la imagen");
			this.publicationsService.downloadImage(this.refImage).then (url => {
				console.log (url);
				this.urlImage = url;
				this.publicationsService.addPublication (this.titulo, this.urlImage, this.listaCategoriasID, this.usuarioActual, this.refImage, this.usernameActual);
			}).catch ( err => console.log ("Error al leer la url de la imagen" + err));
		});
	}

	async abrirCategorias() {
		const modal = await this.modalCtrl.create({
			component: ChooseCategoryPage
		});

		await modal.present();
		const datos = await modal.onDidDismiss();

		this.listaCategoriasID = datos.data.ids;
		this.listaCategorias = datos.data.nombres;

		this.mostrarCategorias = '';
		for (let categorias of this.listaCategorias) {
			this.mostrarCategorias += " " + categorias;
		}

		//console.log (this.mostrarCategorias);
	}

}
