import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPublicationPageRoutingModule } from './add-publication-routing.module';

import { AddPublicationPage } from './add-publication.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ChooseCategoryPage } from '../../categories/choose-category/choose-category.page';
import { ChooseCategoryPageModule } from '../../categories/choose-category/choose-category.module';

@NgModule({
	entryComponents: [ChooseCategoryPage],
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		AddPublicationPageRoutingModule,
		ComponentsModule,
		ChooseCategoryPageModule
	],
	declarations: [AddPublicationPage]
})
export class AddPublicationPageModule { }
