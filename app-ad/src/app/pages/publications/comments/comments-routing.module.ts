import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommentsPage } from './comments.page';

@NgModule({
  imports: [],
  exports: [RouterModule],
})
export class CommentsPageRoutingModule {}
