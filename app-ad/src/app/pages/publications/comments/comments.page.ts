import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { PublicationsService } from 'src/app/services/publications.service';
import { PublicationI } from 'src/app/interfaces/publication';
import { UsersService } from 'src/app/services/users.service';
import { UserI } from 'src/app/interfaces/user';

@Component({
	selector: 'app-comments',
	templateUrl: './comments.page.html',
	styleUrls: ['./comments.page.scss'],
})
export class CommentsPage implements OnInit {

	public comentario: string;
	public publicacion: PublicationI;
	public usuario: UserI;
	public comments: any[];
	public username: string;
	public idactual: string;
	public user: string;

	@Input() idpublicacion;

	constructor(private modalCtrl: ModalController, private authSvc: AuthService,
		private publicationSvc: PublicationsService, private usersSvc: UsersService) {
		this.idactual = this.authSvc.getCurrentID();
	}

	ngOnInit() {
		this.publicationSvc.getPublication(this.idpublicacion).subscribe(publicacion => {
			this.publicacion = publicacion as PublicationI;
			this.user = this.publicacion.idusuario;
			this.comments = this.publicacion.comentarios;
		});
	}

	cerrarComentarios() {
		this.modalCtrl.dismiss();
	}

	nuevoComentario() {
		this.usersSvc.getCurrentUser().subscribe(usuario => {
			this.usuario = usuario as UserI;
			this.username = this.usuario.usuario;

			this.publicationSvc.newComment(this.comentario, this.username, this.idpublicacion);
			this.comentario = "";
		});
	}

	borrarComentario(texto: string, usuario: string, fecha: string) {

		this.publicationSvc.deleteComment(texto, usuario, fecha, this.idpublicacion);
	}

}
