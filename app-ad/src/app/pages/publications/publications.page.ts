import { Component, OnInit } from '@angular/core';
import { PublicationsService } from '../../services/publications.service';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-publications',
	templateUrl: './publications.page.html',
	styleUrls: ['./publications.page.scss'],
})
export class PublicationsPage implements OnInit {

	public publications: any[];
	public idusuario: string;

	constructor(private publicationsService: PublicationsService, private alertCtrl: AlertController,
			private route: ActivatedRoute) {
		this.idusuario = this.route.snapshot.paramMap.get('id');
		this.publicationsService.getPublicationsUser(this.idusuario).subscribe(publicaciones => {

			this.publications = publicaciones;
			console.log(this.publications);

		});
	}

	ngOnInit() {
	}

	async borrarPublicacion(id: string) {
		const alert = await this.alertCtrl.create ({
			header: 'Operación satisfactoria',
			message: 'Publicación borrada correctamente',
			buttons: ['Aceptar']
		})
		const confirm = await this.alertCtrl.create({
			header: 'Confirmación',
			message: '¿Seguro que quieres borrar esta publicación?',
			buttons: [
				{
					text: 'Cancelar',
					role: 'cancel',
					cssClass: 'secondary',
					handler: () => {
					  console.log('Operación cancelada');
					}
				    }, 
				    {
					text: 'Ok',
					handler: () => {
						this.publicationsService.deletePublication(id);
						alert.present();
					}
				    }
			]
		});

		await confirm.present();
	}


}
