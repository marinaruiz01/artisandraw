import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicationsPage } from './publications.page';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: PublicationsPage
  },
  {
    path: 'publication',
    loadChildren: () => import('./publication/publication.module').then( m => m.PublicationPageModule),
    canActivate : [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicationsPageRoutingModule {}
