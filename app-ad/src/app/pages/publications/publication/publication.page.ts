import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PublicationsService } from 'src/app/services/publications.service';
import { PublicationI } from 'src/app/interfaces/publication';
import { UserI } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/services/users.service';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { CommentsPage } from '../comments/comments.page';
import { ChooseFolderPage } from '../../folders/choose-folder/choose-folder.page';

@Component({
	selector: 'app-publication',
	templateUrl: './publication.page.html',
	styleUrls: ['./publication.page.scss'],
})
export class PublicationPage implements OnInit {
	public id_publication: string;
	public id_usuario_actual: string;
	public publication: PublicationI;
	public likeExiste: any;

	public id_user: string;
	public user: UserI;
	public username: string;
	public date: string;
	public title: string;
	public comments: number;
	public likes: number;
	public image: string;

	public listaCarpetas: string[];

	constructor(private route: ActivatedRoute, private publicationsService: PublicationsService,
		private usersService: UsersService, private actionSheetCtrl: ActionSheetController,
		private router: Router, private authService: AuthService, private modalCtrl: ModalController) { }

	ngOnInit() {
		this.id_publication = this.route.snapshot.paramMap.get('id');
		this.publicationsService.updatePublication(this.id_publication, { id: this.id_publication});
		this.publicationsService.getPublication(this.id_publication).subscribe(publicacion => {
			this.publication = publicacion as PublicationI;
			this.id_user = this.publication.idusuario;
			this.usersService.getUser(this.id_user).subscribe(usuario => {
				this.user = usuario as UserI;
				this.username = this.user.usuario;
			});
			this.date = this.publication.fecha;
			this.title = this.publication.titulo;
			this.comments = this.publication.comentarios.length;
			this.likes = this.publication.likes.length;
			this.image = this.publication.imagen;
		});
		this.id_usuario_actual = this.authService.getCurrentID();
		this.existeLike();

	}

	async mostrarAcciones() {
		const actionSheet = await this.actionSheetCtrl.create({
			header: 'Acciones',
			buttons: [{
				text: 'Borrar',
				role: 'destructive',
				icon: 'trash-outline',
				handler: () => {
					this.publicationsService.deletePublication(this.id_publication);
					alert ("Publicación borrada correctamente");
					this.router.navigate(['/publications']);
				}
			}, {
				text: 'Archivar',
				icon: 'folder-open-outline',
				handler: () => {
					this.abrirModalArchivar();
				}
			}]
		});
		await actionSheet.present();
	}

	async darLike() {
		
		if (this.likeExiste) {
			await this.publicationsService.deleteLike(this.id_publication, this.id_usuario_actual);
			this.existeLike();
		} else {
			await this.publicationsService.setLike(this.id_publication, this.id_usuario_actual);
			this.existeLike();
		}

	}

	async existeLike() {
		this.likeExiste = await this.publicationsService.likeExists(this.id_publication, this.id_usuario_actual);
	}

	async comentar () {
		const modal = await this.modalCtrl.create({
			component: CommentsPage,
			componentProps: {
				idpublicacion: this.id_publication
			}
		});

		await modal.present();
	}

	async abrirModalArchivar () {
		const modal = await this.modalCtrl.create({
			component: ChooseFolderPage,
			componentProps: {
				idusuario: this.id_usuario_actual,
				idpublicacion: this.id_publication
			}
		});

		await modal.present();
		const datos = await modal.onDidDismiss();

		this.listaCarpetas = datos.data.carpetas;

	}


}
