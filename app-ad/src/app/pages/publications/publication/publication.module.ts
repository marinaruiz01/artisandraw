import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicationPageRoutingModule } from './publication-routing.module';

import { PublicationPage } from './publication.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { CommentsPage } from '../comments/comments.page';
import { CommentsPageModule } from '../comments/comments.module';
import { ChooseFolderPageModule } from '../../folders/choose-folder/choose-folder.module';
import { ChooseFolderPage } from '../../folders/choose-folder/choose-folder.page';

@NgModule({
	entryComponents: [
		CommentsPage,
		ChooseFolderPage
	],
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		PublicationPageRoutingModule,
		ComponentsModule,
		CommentsPageModule,
		ChooseFolderPageModule
	],
	declarations: [PublicationPage]
})
export class PublicationPageModule { }
