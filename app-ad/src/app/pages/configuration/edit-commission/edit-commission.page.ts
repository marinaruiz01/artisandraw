import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-edit-commission',
	templateUrl: './edit-commission.page.html',
	styleUrls: ['./edit-commission.page.scss'],
})
export class EditCommissionPage implements OnInit {
	public idactual: string;
	public user: any;
	public comisiones: boolean;

	constructor(private authSvc: AuthService, private userSvc: UsersService, private router: Router) {
		this.idactual = this.authSvc.getCurrentID();
		this.userSvc.getCurrentUser().subscribe( user => {
			this.user = user;
			this.comisiones = this.user.comisiones_activas;
		});
	}

	ngOnInit() {
	}

	activar() {
		this.userSvc.editUser(this.idactual, { comisiones_activas: true });
		alert ("Comisiones activadas correctamente.");
		this.router.navigate(['/configuration/edit-commission']);
	}

	desactivar() {
		this.userSvc.editUser(this.idactual, { comisiones_activas: false });
		alert ("Comisiones desactivadas correctamente.");
		this.router.navigate(['/configuration/edit-commission']);

	}

}
