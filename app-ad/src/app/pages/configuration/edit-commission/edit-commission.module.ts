import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCommissionPageRoutingModule } from './edit-commission-routing.module';

import { EditCommissionPage } from './edit-commission.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditCommissionPageRoutingModule,
    ComponentsModule
  ],
  declarations: [EditCommissionPage]
})
export class EditCommissionPageModule {}
