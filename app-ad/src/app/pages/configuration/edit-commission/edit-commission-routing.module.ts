import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCommissionPage } from './edit-commission.page';

const routes: Routes = [
  {
    path: '',
    component: EditCommissionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCommissionPageRoutingModule {}
