import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPassPageRoutingModule } from './edit-pass-routing.module';

import { EditPassPage } from './edit-pass.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPassPageRoutingModule,
    ComponentsModule
  ],
  declarations: [EditPassPage]
})
export class EditPassPageModule {}
