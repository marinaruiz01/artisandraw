import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-edit-pass',
  templateUrl: './edit-pass.page.html',
  styleUrls: ['./edit-pass.page.scss'],
})
export class EditPassPage implements OnInit {

	public iduser: string;
	public pass: string;
	public newpass: string;
	public newpass0: string;

  constructor(private authSvc: AuthService) { 
	  this.iduser = this.authSvc.getCurrentID();
  }

  ngOnInit() {
  }

  modificar() {

	if (this.newpass != this.newpass0) {
		alert ("Error al confirmar la nueva contraseña, no coinciden.");
	} else {
		this.authSvc.editPass(this.pass, this.newpass);
	}
  }

}
