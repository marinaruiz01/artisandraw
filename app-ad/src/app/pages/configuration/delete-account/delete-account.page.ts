import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { PublicationsService } from 'src/app/services/publications.service';
import { FoldersService } from 'src/app/services/folders.service';
import { ProductsService } from 'src/app/services/products.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-delete-account',
	templateUrl: './delete-account.page.html',
	styleUrls: ['./delete-account.page.scss'],
})
export class DeleteAccountPage implements OnInit {

	public pass: string;
	public iduser: string;
	public user: any;
	public siguiendo: Array<string>;
	public seguidores: Array<string>;
	public publicaciones: any[];
	public productos: any[];

	constructor(private authSvc: AuthService, private userSvc: UsersService, private publSvc: PublicationsService,
		private folderSvc: FoldersService, private prodSvc: ProductsService/*, private router: Router*/) { 
		
	}

	ngOnInit() {
		this.iduser = this.authSvc.getCurrentID();
	}

	borrar() {
		if (confirm("¿Estás seguro de que quieres borrar tu cuenta?")) {
			this.userSvc.getCurrentUser().subscribe( user => {
				this.user = user;
				this.siguiendo = this.user.siguiendo;
				this.seguidores = this.user.seguidores;
				for (let usuario of this.siguiendo) {
					this.userSvc.cleanFollows(this.iduser, usuario);
				}
				for (let usuario of this.seguidores) {
					this.userSvc.cleanFollows(this.iduser, usuario);
				}

				this.publSvc.getPublicationsUser(this.iduser).subscribe ( publications => {
					this.publicaciones = publications;
					for (let publicacion of this.publicaciones) {
						this.publSvc.deletePublication(publicacion.id);
						this.folderSvc.cleanFolders(publicacion.id);
					}
					this.prodSvc.getProductsUser(this.iduser).subscribe ( products => {
						this.productos = products;
						for (let producto of this.productos) {
							this.prodSvc.deleteProduct(producto.id);
						}
					})
					this.userSvc.deleteUser(this.iduser);
					this.authSvc.deleteUser(this.pass);
					//this.router.navigate(['/login']);
				})
			})
		}
	}

}
