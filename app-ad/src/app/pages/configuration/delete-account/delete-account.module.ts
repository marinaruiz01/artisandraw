import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeleteAccountPageRoutingModule } from './delete-account-routing.module';

import { DeleteAccountPage } from './delete-account.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { AngularFireAuthModule } from '@angular/fire/auth';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeleteAccountPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ DeleteAccountPage ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class DeleteAccountPageModule {}
