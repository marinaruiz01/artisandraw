import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReplyPageRoutingModule } from './reply-routing.module';

import { ReplyPage } from './reply.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReplyPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ReplyPage]
})
export class ReplyPageModule {}
