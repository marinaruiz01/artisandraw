import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessagesService } from 'src/app/services/messages.service';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-reply',
  templateUrl: './reply.page.html',
  styleUrls: ['./reply.page.scss'],
})
export class ReplyPage implements OnInit {
	public idrespondido: string;
	public respondido: any;
	public asunto: string;
	public texto_anterior: string;
	public usuario: string;
	public idusuario: string;
	public emisor: string;
	public idemisor: string;
	public user: any;
	public mensaje: string;

  constructor(private route: ActivatedRoute, private msgSvc: MessagesService, private userSvc: UsersService,
		private authSvc: AuthService) { 
	this.idrespondido = this.route.snapshot.paramMap.get('id');
	this.msgSvc.getMessage(this.idrespondido).subscribe( msg => {
		this.respondido = msg;
		this.asunto = "Re: "+this.respondido.asunto;
		this.usuario = this.respondido.emisor;
		this.idusuario = this.respondido.idemisor;
		this.texto_anterior = this.respondido.texto;
	})
	this.userSvc.getCurrentUser().subscribe( user => {
		this.user = user;
		this.emisor = this.user.usuario;
	})
	this.idemisor = this.authSvc.getCurrentID();
  }

  ngOnInit() {
  }

  enviarRespuesta() {
	this.msgSvc.newMessage(this.idemisor, this.emisor, this.idusuario, this.asunto, this.mensaje, this.idrespondido);
  }

}
