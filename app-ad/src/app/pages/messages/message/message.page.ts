import { Component, OnInit } from '@angular/core';
import { MessagesService } from 'src/app/services/messages.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {
	public idmensaje: string;
	public mensaje: any;
	public emisor: string;
	public asunto: string;
	public texto: string;

  constructor(private msgSvc: MessagesService, private route: ActivatedRoute) { 
	this.idmensaje = this.route.snapshot.paramMap.get('id');
	this.msgSvc.getMessage(this.idmensaje).subscribe( msj => {
		this.mensaje = msj;
		this.emisor = this.mensaje.emisor;
		this.asunto = this.mensaje.asunto;
		this.texto = this.mensaje.texto;
	})
  }

  ngOnInit() {
  }

}
