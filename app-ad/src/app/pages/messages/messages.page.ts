import { Component, OnInit } from '@angular/core';
import { MessagesService } from 'src/app/services/messages.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-messages',
	templateUrl: './messages.page.html',
	styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {

	public mensajes: any[];
	public idusuario: string;

	constructor(private msgSvc: MessagesService, private authSvc: AuthService) {
		this.idusuario = this.authSvc.getCurrentID();
		this.msgSvc.getMessagesUser(this.idusuario).subscribe(messages => {
			this.mensajes = messages;
		});
	}

	ngOnInit() {
	}

	delete(id: string) {
		this.msgSvc.deleteMessage(id);
	}

}
