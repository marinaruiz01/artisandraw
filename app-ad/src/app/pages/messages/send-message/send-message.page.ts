import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { MessagesService } from 'src/app/services/messages.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.page.html',
  styleUrls: ['./send-message.page.scss'],
})
export class SendMessagePage implements OnInit {
	public idusuarioreceptor: string;
	public user: any;
	public username: string;
	public idusuarioemisor: string;
	public mensaje: string;
	public asunto: string;
	public emisor: any;
	public usuarioemisor: string;

  constructor(private route: ActivatedRoute, private usersSvc: UsersService, private msgSvc: MessagesService,
		private authSvc: AuthService) { 
	this.idusuarioreceptor = this.route.snapshot.paramMap.get('iduser');
	this.usersSvc.getUser(this.idusuarioreceptor).subscribe ( user => {
		this.user = user;
		this.username = this.user.usuario;
	})
	this.idusuarioemisor = this.authSvc.getCurrentID();
	this.usersSvc.getCurrentUser().subscribe ( user => {
		this.emisor = user;
		this.usuarioemisor = this.emisor.usuario;
	})
  }

  ngOnInit() {
  }

  enviarMensaje () {
	this.msgSvc.newMessage(this.idusuarioemisor, this.usuarioemisor, this.idusuarioreceptor, this.asunto, this.mensaje, "");
  }

}
