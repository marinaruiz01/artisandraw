import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { CommentsPage } from '../publications/comments/comments.page';
import { ChooseFolderPage } from '../folders/choose-folder/choose-folder.page';
import { CommentsPageModule } from '../publications/comments/comments.module';
import { ChooseFolderPageModule } from '../folders/choose-folder/choose-folder.module';

@NgModule({
	entryComponents: [
		CommentsPage,
		ChooseFolderPage
	],
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		HomePageRoutingModule,
		ComponentsModule,
		CommentsPageModule,
		ChooseFolderPageModule
	],
	declarations: [HomePage]
})
export class HomePageModule { }
