import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Router } from '@angular/router';
import { FoldersService } from 'src/app/services/folders.service';
import { PublicationsService } from 'src/app/services/publications.service';
import { UsersService } from 'src/app/services/users.service';
import * as firebase from 'firebase/app';
import { ProductsService } from 'src/app/services/products.service';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { ChooseFolderPage } from '../folders/choose-folder/choose-folder.page';
import { CommentsPage } from '../publications/comments/comments.page';

@Component({
	selector: 'app-home',
	templateUrl: './home.page.html',
	styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

	public usuarioActual: string;
	public publications: any;
	public useractual: any;
	public siguiendo: Array<string> = [];
	public allPublications: Array<any> = [];

	public usuario: any;
	public username: string;
	public listaCarpetas: string[];

	constructor(private authSvc: AuthService, private router: Router,
		private foldersSvc: FoldersService, private publicationsSvc: PublicationsService,
		private userSvc: UsersService, private actionSheetCtrl: ActionSheetController,
		private modalCtrl: ModalController) {

		this.userSvc.getUserActual().then(user => {
			this.useractual = user;
			this.siguiendo = this.useractual.siguiendo;
			if (this.siguiendo.length < 1) {
				this.publicationsSvc.getPublications().subscribe(publicaciones => {
					this.publications = publicaciones;
					this.allPublications = [];
					for (let publicacion of this.publications) {
						this.allPublications.push(publicacion);
					}
				});
			} else {
				this.allPublications = [];
				for (let idusuario of this.siguiendo) {
					this.publicationsSvc.getPublUser(idusuario).then(publicaciones => {
						this.publications = publicaciones;
						for (let publicacion of this.publications) {
							this.allPublications.push(publicacion);
						}
					});
				}
			}
		});

	}

	ngOnInit() {
		this.usuarioActual = this.authSvc.getCurrentID();


	}

	onLogout() {
		this.authSvc.logout();
	}

	irAddPublication() {
		this.router.navigate(['/publications/add-publication']);
	}

	irAddProduct() {
		this.router.navigate(['/products/add-product']);
	}

	addFolder() {
		const carpeta = prompt("Nombre para la nueva carpeta");

		if (carpeta.length > 0) {
			this.foldersSvc.createFolder(carpeta, this.usuarioActual);
		}
	}

}
