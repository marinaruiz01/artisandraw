import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  public email: string;
  public pass: string;

  constructor(private auth: AuthService, private router: Router, private ngZone: NgZone) { 
  }

  ngOnInit() {
  }

  onSubmitLogin() {
    return this.auth.login (this.email, this.pass)
	    .then ( res => {
		    console.log ("Se ha iniciado sesión correctamente.");
		    this.ngZone.run(() => this.router.navigate(['/home']));
		    return true;
		})
	    .catch ( err => {
		    alert ("Ha habido un error al iniciar sesión." + err);
		    return false;
	});
  }

}
