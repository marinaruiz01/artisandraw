import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import { LoginPage } from './login.page';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RouterModule, Routes } from '@angular/router';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HomePage } from '../home/home.page';
import { HomePageModule } from '../home/home.module';

describe( 'LoginPage', () => {
	let component: LoginPage;
	let fixture: ComponentFixture<LoginPage>;

	const routes: Routes = [
		{ path: 'home', component: HomePage }
	];
	beforeEach(async () => {
		TestBed.configureTestingModule({
			declarations: [LoginPage],
			imports: [
				FormsModule,
				AngularFireModule.initializeApp(firebaseConfig),
				AngularFireAuthModule,
				AngularFirestoreModule,
				HomePageModule,
				RouterModule.forRoot(routes)
			],
			schemas: [CUSTOM_ELEMENTS_SCHEMA],
		}).compileComponents();
	});

	beforeEach( () => {
		fixture = TestBed.createComponent(LoginPage);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should login', () => {
		component.email = 'pruebas@gmail.com';
		component.pass = 'asdfgh';
		expect(component.onSubmitLogin()).toBeTruthy();
	});
})