import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-shop',
	templateUrl: './shop.page.html',
	styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {
	public products: any[];
	public user: any;
	public idusuario: string;
	public verificado: boolean;
	public comisiones: boolean;

	constructor(private productsService: ProductsService, private usersService: UsersService,
		private route: ActivatedRoute) {

		this.idusuario = this.route.snapshot.paramMap.get('id');
		this.usersService.getUser(this.idusuario).subscribe(usuario => {
			this.user = usuario;
			this.verificado = this.user.verificada;
			this.comisiones = this.user.comisiones_activas;
		})
		this.productsService.getProductsUser(this.idusuario).subscribe(productos => {
			this.products = productos;
		});
	}

	ngOnInit() {

	}

}
