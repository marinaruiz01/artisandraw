import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-choose-category',
	templateUrl: './choose-category.page.html',
	styleUrls: ['./choose-category.page.scss'],
})
export class ChooseCategoryPage implements OnInit {

	public categories: any[];
	public listaCategoriasID: Array<String> = [];
	public listaCategorias: Array<String> = [];

	constructor(private categoriesService: CategoriesService, private modalCtrl: ModalController) { 

		this.categoriesService.getCategories().subscribe(categories => {
			this.categories = categories;

			for (let category of this.categories) {
				Object.defineProperty (category, 'checked', {
					value: false,
					writable: true
				});
			}
		});
	}

	ngOnInit() {
	}

	cerrarModal() {

		for (let categoria of this.categories) {
			if (categoria.checked) {
				this.listaCategoriasID.push(categoria.id);
				this.listaCategorias.push(categoria.nombre);
			}
		}

		this.modalCtrl.dismiss({
			nombres: this.listaCategorias,
			ids: this.listaCategoriasID
		});
	}
}
