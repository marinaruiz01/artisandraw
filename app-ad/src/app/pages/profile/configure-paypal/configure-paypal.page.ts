import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { UsersService } from 'src/app/services/users.service';

@Component({
	selector: 'app-configure-paypal',
	templateUrl: './configure-paypal.page.html',
	styleUrls: ['./configure-paypal.page.scss'],
})
export class ConfigurePaypalPage implements OnInit {

	public paypal: string;
	public num_verificacion: number;
	public cantidad: number;
	public auth_paypal: any;
	public user: any;
	public verificado: boolean;

	constructor(private usersSvc: UsersService, private http: HttpClient) {
		this.cantidad = Math.round((Math.random() * (0.5 - 0.01) + 0.01) * 100) / 100;
		this.obtenerAccessToken();
		this.usersSvc.getCurrentUser().subscribe ( user => {
			this.user = user;
			this.verificado = this.user.verificada;
		})
	}

	ngOnInit() {
	}

	obtenerAccessToken() {
		const body = new HttpParams().set('grant_type', 'client_credentials');
		const headers = new HttpHeaders().append('Authorization', "Basic " + btoa("AbqmTyzUauctATs-bFXvU70RZyHfbRePeOQyBkKcEJG03tsudZhGXyLha84k2AcuqfonBARHD3bWPT-3:EAIsJfcCgxX7aon3SJ9xpgr0g_Nq7YuI-gGy6L8-3gN2C9qcgkqkXMF-lLgjorbF2oLdHACaEdkOSHpX"));
		this.http.post("https://api.sandbox.paypal.com/v1/oauth2/token",
			body,
			{ headers })
			.subscribe(
				(val) => {
					//console.log("POST call successful value returned in body", val);
					this.auth_paypal = val;
				},
				response => {
					console.log("POST call in error", response);
				},
				() => {
					console.log("The POST observable is now completed.");
				});
	}

	transferencia() {
		//console.log(this.auth_paypal.access_token);
		const headers = new HttpHeaders().set("Authorization", "Bearer " + this.auth_paypal.access_token);
		headers.set("Content-Type", "application/json");
		var sender_batch_id = Math.random().toString(36).substring(9);
		this.http.post("https://api.sandbox.paypal.com/v1/payments/payouts",
			{
				"sender_batch_header": {
					"sender_batch_id": sender_batch_id,
					"email_subject": "Pago de verificacion AD",
					"email_message": "Este es el pago de verificación de tu cuenta en ArtisanDraw"
				},
				"items": [

					{
						"recipient_type": "EMAIL",
						"amount": {
							"value": this.cantidad,
							"currency": "EUR"
						},
						"note": "Thanks for your support!",
						"sender_item_id": "201403140002",
						"receiver": this.paypal
					}
				]

			}, { headers })
			.subscribe(
				(val) => {
					//console.log("POST call successful value returned in body", val);
					alert ("Transferencia realizada. Por favor, comprueba en tu cuenta de PayPal la cantidad recibida.")
				},
				response => {
					console.log("POST call in error", response);
				},
				() => {
					console.log("The POST observable is now completed.");
				});
	}

	verificar() {
		if (this.num_verificacion == this.cantidad) {
			this.usersSvc.editCurrentUser({ paypal: this.paypal, verificada: true })
			alert ("Su cuenta ha sido verificada correctamente");
		} else {
			console.log ("Error al verificar la cuenta");
		}
	}

}
