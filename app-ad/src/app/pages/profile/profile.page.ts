import { Component, OnInit } from '@angular/core';
import { UsersService } from "../../services/users.service";
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

@Component({
	selector: 'app-profile',
	templateUrl: './profile.page.html',
	styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

	public user: any;
	public name: string;
	public username: string;
	public email: string;
	public rol: string;
	public calificacion_vendedor: number;
	public calificacion_comprador: number;
	public seguidores: number;
	public seguidos: number;
	public comisiones: boolean;

	public comision: string;
	public userActual: string;
	public activarSeguir: boolean;
	public followExiste: any;
	public monedero: number;
	public verificada: boolean;
	public mostrarMonedero: boolean;
	public auth_paypal: any;

	// Paypal
	public requestBody;
	public sync_mode = true;

	constructor(private users_service: UsersService, private route: ActivatedRoute,
		private auth_service: AuthService, private http: HttpClient, private router: Router) {
		this.obtenerUsuarioActual();

		if (this.userActual == 'me') {
			this.activarSeguir = false;
		} else {
			this.activarSeguir = true;
		}

		this.existeFollow();
		this.obtenerAccessToken();

	}

	ngOnInit() {
		
	}

	obtenerUsuarioActual() {
		this.userActual = this.route.snapshot.paramMap.get('id');
		if (this.userActual == 'me') {
			this.users_service.getCurrentUser()
				.subscribe(user => {
					this.user = user;
					this.name = this.user.nombre;
					this.monedero = this.user.monedero;
					this.username = this.user.usuario;
					this.comisiones = this.user.comisiones_activas;
					this.calificacion_comprador = this.user.calificacion_comprador;
					this.calificacion_vendedor = this.user.calificacion_vendedor;
					this.seguidores = this.user.seguidores.length;
					this.seguidos = this.user.siguiendo.length;
					this.verificada = this.user.verificada;
					if (this.verificada) {
						this.mostrarMonedero = true;
					} else {
						this.mostrarMonedero = false;
					}

					this.userActual = this.auth_service.getCurrentID();
					this.users_service.editUser(this.userActual, { id: this.userActual });
					this.comisionesActivas();
				});
		} else {
			this.users_service.getUser(this.userActual)
				.subscribe(user => {
					this.user = user;
					this.name = this.user.nombre;
					this.username = this.user.usuario;
					this.comisiones = this.user.comisiones_activas;
					this.calificacion_comprador = this.user.calificacion_comprador;
					this.calificacion_vendedor = this.user.calificacion_vendedor;
					this.seguidores = this.user.seguidores.length;
					this.seguidos = this.user.siguiendo.length;
					this.monedero = this.user.monedero;
					this.verificada = this.user.verificada;
					this.mostrarMonedero = false;

					this.users_service.editUser(this.userActual, { id: this.userActual });
					this.comisionesActivas();
				});
		}

		this.existeFollow();

	}

	comisionesActivas() {
		if (this.comisiones == true) {
			this.comision = "Activadas";
		} else {
			this.comision = "Desactivadas";
		}
	}

	async existeFollow() {
		this.followExiste = await this.users_service.followExists(this.userActual);
	}

	async seguirUsuario() {
		if (!this.followExiste) {
			await this.users_service.followUser(this.userActual);
			await this.existeFollow();
		} else {
			await this.users_service.unfollowUser(this.userActual);
			await this.existeFollow();
		}
	}

	obtenerAccessToken() {
		const body = new HttpParams().set('grant_type', 'client_credentials');
		const headers = new HttpHeaders().append('Authorization', "Basic " + btoa("AbqmTyzUauctATs-bFXvU70RZyHfbRePeOQyBkKcEJG03tsudZhGXyLha84k2AcuqfonBARHD3bWPT-3:EAIsJfcCgxX7aon3SJ9xpgr0g_Nq7YuI-gGy6L8-3gN2C9qcgkqkXMF-lLgjorbF2oLdHACaEdkOSHpX"));
		this.http.post("https://api.sandbox.paypal.com/v1/oauth2/token",
			body,
			{ headers })
			.subscribe(
				(val) => {
					//console.log("POST call successful value returned in body", val);
					this.auth_paypal = val;
				},
				response => {
					console.log("POST call in error", response);
				},
				() => {
					console.log("The POST observable is now completed.");
				});
	}

	canjear() {
		const headers = new HttpHeaders().set("Authorization", "Bearer "+this.auth_paypal.access_token);
		headers.set("Content-Type", "application/json");
		var sender_batch_id = Math.random().toString(36).substring(9);
		this.http.post("https://api.sandbox.paypal.com/v1/payments/payouts",
			{
				"sender_batch_header": {
					"sender_batch_id": sender_batch_id,
					"email_subject": "Pago de ArtisanDraw",
					"email_message": "¡Has recibido tu pago! Muchas gracias por confiar en nosotros."
				},
				"items": [

					{
						"recipient_type": "EMAIL",
						"amount": {
							"value": this.monedero*0.95,
							"currency": "EUR"
						},
						"note": "Thanks for your support!",
						"sender_item_id": "201403140002",
						"receiver": "sb-t74gb1955791@personal.example.com"
					}
				]

			}, { headers })
			.subscribe(
				(val) => {
					//console.log("POST call successful value returned in body", val);
					alert ("Pago recibido correctamente.");
					this.monedero = 0;
					this.users_service.editUser(this.userActual, { monedero: this.monedero});
				},
				response => {
					console.log("POST call in error", response);
				},
				() => {
					//console.log("The POST observable is now completed.");
				});
	}

}
