import { Component, OnInit } from '@angular/core';
import { UsersService } from "../../../services/users.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'app-edit',
	templateUrl: './edit.page.html',
	styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

	public user: any;
	public name: string;
	public username: string;
	public email: string;
	public rol: string;
	public calificacion_vendedor: number;
	public calificacion_comprador: number;
	public seguidores: number;
	public seguidos: number;
	public comisiones: boolean;
	public datos: any;
	public pass: string;

	public archivo: File;
	public refImage: string;

	// Métodos

	constructor(private users_service: UsersService, private router: Router) {
		this.obtenerUsuario();
	}

	ngOnInit() {
	}

	obtenerUsuario() {
		this.users_service.getCurrentUser()
			.subscribe(user => {
				this.user = user;
				this.name = this.user.nombre;
				this.username = this.user.usuario;
				this.rol = this.user.rol;
				this.email = this.user.email;
				this.comisiones = this.user.comisiones_activas;
				this.calificacion_comprador = this.user.calificacion_comprador;
				this.calificacion_vendedor = this.user.calificacion_vendedor;
				this.seguidores = this.user.num_seguidores;
				this.seguidos = this.user.num_seguidos;
				this.pass = this.user.pass;
			});
	}

	modificarPerfil() {
		this.datos = {
			usuario: this.username,
			email: this.email,
			nombre: this.name,
		}

		this.users_service.editCurrentUser(this.datos);
		alert("Perfil modificado correctamente");
		this.router.navigate(['/profile']);
	}

	setFile(event) {
		var file = event.target.files;
		this.archivo = file[0];
		console.log(this.archivo);
	}

	uploadFile() {
		this.refImage = this.username + "profile";
		this.users_service.uploadAvatar(this.refImage, this.archivo);
	}

	/*downloadFile() {
		this.refImage = this.username + "profile";
		this.users_service.downloadAvatar(this.refImage).then (url => {
			console.log (url);
			document.getElementById('imgAvatar').setAttribute('src', url);
		});
	}*/


}
