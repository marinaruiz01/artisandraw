import { Component, OnInit } from '@angular/core';
import { CommissionI } from 'src/app/interfaces/commission';
import { ActivatedRoute } from '@angular/router';
import { CommissionsService } from 'src/app/services/commissions.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
	selector: 'app-orders-user',
	templateUrl: './orders-user.page.html',
	styleUrls: ['./orders-user.page.scss'],
})
export class OrdersUserPage implements OnInit {

	public idusuario: string;
	public pedidos: CommissionI[];

	constructor(private route: ActivatedRoute, private commSvc: CommissionsService) {
		this.idusuario = this.route.snapshot.paramMap.get('iduser');
		this.commSvc.getCommissionsUser(this.idusuario).subscribe(comm => {
			this.pedidos = comm;
		})
	}

	ngOnInit() {
	}

}
