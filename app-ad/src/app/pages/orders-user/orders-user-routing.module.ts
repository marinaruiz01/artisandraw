import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersUserPage } from './orders-user.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersUserPageRoutingModule {}
