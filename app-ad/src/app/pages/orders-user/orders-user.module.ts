import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersUserPageRoutingModule } from './orders-user-routing.module';

import { OrdersUserPage } from './orders-user.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersUserPageRoutingModule,
    ComponentsModule
  ],
  declarations: [OrdersUserPage]
})
export class OrdersUserPageModule {}
