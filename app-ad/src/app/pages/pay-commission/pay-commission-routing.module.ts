import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayCommissionPage } from './pay-commission.page';

const routes: Routes = [
  {
    path: '',
    component: PayCommissionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayCommissionPageRoutingModule {}
