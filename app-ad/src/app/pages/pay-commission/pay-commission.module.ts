import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayCommissionPageRoutingModule } from './pay-commission-routing.module';

import { PayCommissionPage } from './pay-commission.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayCommissionPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PayCommissionPage]
})
export class PayCommissionPageModule {}
