import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommissionsService } from 'src/app/services/commissions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';
import { CommissionI } from 'src/app/interfaces/commission';
import { UserI } from 'src/app/interfaces/user';

declare var paypal;

@Component({
	selector: 'app-pay-commission',
	templateUrl: './pay-commission.page.html',
	styleUrls: ['./pay-commission.page.scss'],
})
export class PayCommissionPage implements OnInit {

	@ViewChild('paypal', { static: true }) paypalElement: ElementRef;

	public idcommission: string;
	public id_user: string;
	public pedido: any;
	public user: any;
	public price: number;
	public envio: number;

	public addScript: boolean = false;
	public producto: any;
	public paypalConfig: any;

	public monedero: number;
	public iduser: string;
	public direccion: string;

	constructor(private commSvc: CommissionsService, private route: ActivatedRoute, private usersSvc: UsersService,
		private authSvc: AuthService, private router: Router) {
		this.idcommission = this.route.snapshot.paramMap.get('id');
		this.commSvc.getCommission(this.idcommission).subscribe(comm => {
			this.pedido = comm as CommissionI;
			this.id_user = this.pedido.idautor;
			this.price = this.pedido.precio;
			this.envio = this.pedido.envio;
			this.usersSvc.getUser(this.id_user).subscribe(usuario => {
				this.user = usuario as UserI;
				this.iduser = this.user.id;
				this.monedero = this.user.monedero;
			});
			this.paypalConfig = {
				env: 'sandbox',
				client: {
					sandbox: 'AbqmTyzUauctATs-bFXvU70RZyHfbRePeOQyBkKcEJG03tsudZhGXyLha84k2AcuqfonBARHD3bWPT-3',
					production: ''
				},
				commit: true,
				payment: (data, actions) => {
					return actions.payment.create({
						payment: {
							transactions: [
								{
									amount: {
										total: this.price + this.envio,
										currency: 'EUR'
									}
								}
							]
						}
					});
				},
				onAuthorize: (data, actions) => {
					return actions.payment.execute().then(payment => {
						return actions.payment.get().then (data => {
							this.monedero += this.price;
							let datos = {
								monedero: this.monedero
							}
							this.usersSvc.editUser(this.iduser, datos);
							var useractual = this.authSvc.getCurrentID();
							this.commSvc.updateCommission(this.idcommission, { direccion: this.direccion, estado: "Pagado" });
							alert("Pago realizado correctamente. Muchas gracias por confiar en ArtisanDraw.");
							this.router.navigate(['/orders-user/'+useractual]);
						})
					})
				}
			}
			if (!this.addScript) {
				this.addPaypalScript().then(() => {
					paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn')
				})
			}
		})
	}

	ngOnInit() {
	}


	addPaypalScript() {
		this.addScript = true;
		return new Promise((resolve, reject) => {
			let scriptTag = document.createElement('script');
			scriptTag.src = 'https://www.paypalobjects.com/api/checkout.js';
			scriptTag.onload = resolve;
			document.body.appendChild(scriptTag);
		})
	}

}
