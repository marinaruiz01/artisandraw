import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersPageRoutingModule } from './orders-routing.module';

import { OrdersPage } from './orders.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { EditStatusPageModule } from './edit-status/edit-status.module';
import { EditStatusPage } from './edit-status/edit-status.page';

@NgModule({
	entryComponents: [EditStatusPage],
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		OrdersPageRoutingModule,
		ComponentsModule,
		EditStatusPageModule
	],
	declarations: [OrdersPage]
})
export class OrdersPageModule { }
