import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommissionsService } from 'src/app/services/commissions.service';
import { UsersService } from 'src/app/services/users.service';
import { CommissionI } from 'src/app/interfaces/commission';
import { ModalController } from '@ionic/angular';
import { EditStatusPage } from './edit-status/edit-status.page';

@Component({
	selector: 'app-orders',
	templateUrl: './orders.page.html',
	styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
	public idusuario: string;
	public pedidos: CommissionI[];
	public ruta: string;

	public nuevoestado: string;
	public precio: number;
	public envio: number;

	constructor(private route: ActivatedRoute, private commSvc: CommissionsService, private usersSvc: UsersService,
		private router: Router, private modalCtrl: ModalController) {
		this.idusuario = this.route.snapshot.paramMap.get('iduser');
		this.ruta = "/orders/" + this.idusuario + "/order/";
		this.commSvc.getCommissionsAuthor(this.idusuario).subscribe(comm => {
			this.pedidos = comm;
		})
	}

	ngOnInit() {
	}

	irOrder(id: string) {
		console.log(id);
		this.ruta += id;
		this.router.navigate([this.ruta]);
	}

	async modificarEstado(id: string) {
		const modal = await this.modalCtrl.create({
			component: EditStatusPage
		});

		await modal.present();
		const datos = await modal.onDidDismiss();

		this.nuevoestado = datos.data.estado;
		this.precio = datos.data.precio;
		this.envio = datos.data.envio;

		console.log(this.precio);
		if (!this.precio || !this.envio) {
			this.commSvc.updateCommission(id, { estado: this.nuevoestado });
		} else {
			this.commSvc.updateCommission(id, { estado: this.nuevoestado, precio: this.precio, envio: this.envio });
		}
	}

	delete(id: string) {
		this.commSvc.deleteCommission(id);
	}

}
