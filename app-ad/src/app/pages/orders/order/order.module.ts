import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderPageRoutingModule } from './order-routing.module';

import { OrderPage } from './order.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { EditStatusPageModule } from '../edit-status/edit-status.module';
import { EditStatusPage } from '../edit-status/edit-status.page';

@NgModule({
	entryComponents: [EditStatusPage],
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		OrderPageRoutingModule,
		ComponentsModule,
		EditStatusPageModule
	],
	declarations: [OrderPage]
})
export class OrderPageModule { }
