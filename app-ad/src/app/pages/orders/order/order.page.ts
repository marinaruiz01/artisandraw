import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommissionsService } from 'src/app/services/commissions.service';
import { ModalController } from '@ionic/angular';
import { EditStatusPage } from '../edit-status/edit-status.page';

@Component({
	selector: 'app-order',
	templateUrl: './order.page.html',
	styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
	public idorder: string;
	public order: any;

	public tam: string;
	public tipo: string;
	public usuario: string;
	public descripcion: string;
	public estado: string;
	public presupuesto: number;
	public direccion: string = "N/A"

	public nuevoestado: string;
	public precio: string;
	public usuarioid: string;

	constructor(private route: ActivatedRoute, private commSvc: CommissionsService, private modalCtrl: ModalController) {
		this.idorder = this.route.snapshot.paramMap.get('idorder');
		this.commSvc.getCommission(this.idorder).subscribe(comm => {
			this.order = comm;
			this.tam = this.order.tamaño;
			this.tipo = this.order.tipo;
			this.usuario = this.order.usuario;
			this.usuarioid = this.order.idusuario;
			this.descripcion = this.order.descripcion;
			this.estado = this.order.estado;
			this.presupuesto = this.order.presupuesto;
			this.direccion = this.order.direccion;
		})
	}

	ngOnInit() {
	}

	async modificarEstado() {
		const modal = await this.modalCtrl.create({
			component: EditStatusPage
		});

		await modal.present();
		const datos = await modal.onDidDismiss();

		this.nuevoestado = datos.data.estado;
		this.precio = datos.data.precio;

		console.log(this.precio);
		if (!this.precio) {
			console.log("1");
			this.commSvc.updateCommission(this.idorder, { estado: this.nuevoestado });
		} else {
			console.log("2");
			this.commSvc.updateCommission(this.idorder, { estado: this.nuevoestado, precio: this.precio});
		}

	}

}
