import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-edit-status',
	templateUrl: './edit-status.page.html',
	styleUrls: ['./edit-status.page.scss'],
})
export class EditStatusPage implements OnInit {

	public estado: string;
	public precio: number;
	public envio: number;

	constructor(private modalCtrl: ModalController) { }

	ngOnInit() {
	}

	cerrarModal() {
		console.log (this.precio);
		this.modalCtrl.dismiss({
			estado: this.estado,
			precio: this.precio,
			envio: this.envio
		});
	}

}
