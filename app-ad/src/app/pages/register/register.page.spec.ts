import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import { RegisterPage } from './register.page';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from 'src/environments/environment';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { RouterModule, Routes } from '@angular/router';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { ModalController } from '@ionic/angular';
import { HomePage } from '../home/home.page';
import { HomePageModule } from '../home/home.module';

describe( 'RegisterPage', () => {
	let component: RegisterPage;
	let fixture: ComponentFixture<RegisterPage>;
	let modalCtrl: ModalController;
	let angFAuth: AngularFireAuth;

	const routes: Routes = [
		{ path: 'home', component: HomePage }
	];
	beforeEach(async () => {
		TestBed.configureTestingModule({
			declarations: [RegisterPage],
			imports: [
				FormsModule,
				AngularFireModule.initializeApp(firebaseConfig),
				AngularFireAuthModule,
				AngularFirestoreModule,
				AngularFireStorageModule,
				HomePageModule,
				RouterModule.forRoot(routes)
			],
			providers: [
				{ provide: ModalController }
			],
			schemas: [CUSTOM_ELEMENTS_SCHEMA],
		}).compileComponents();
		modalCtrl = TestBed.get(ModalController);
	});

	beforeEach( () => {
		fixture = TestBed.createComponent(RegisterPage);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should register', () => {
		component.email = "correodeprueba@gmail.com";
		component.user = "userprueba";
		component.name = "Usuario de prueba";
		component.pass = "123456";
		expect(component.onSubmitRegister()).toBeTruthy(); 
	});
})