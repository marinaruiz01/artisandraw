import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
	selector: 'app-register',
	templateUrl: './register.page.html',
	styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
	public email: string;
	public pass: string;
	public name: string;
	public user: string;
	public confirmar_pass: string;

	constructor(private auth: AuthService, private router: Router, private ngZone: NgZone) {
	}

	ngOnInit() {
	}

	onSubmitRegister() {
		return this.auth.register(this.email, this.pass, this.name, this.user)
			.then(user => {
				alert("¡Bienvenido/a, ya formas parte de ArtisanDraw!")
				this.ngZone.run(() => this.router.navigate(['home']));
				return true;
			})
			.catch(err => {
				alert(err.message);
				return false;
			})
	}

}
