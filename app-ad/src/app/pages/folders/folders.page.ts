import { Component, OnInit } from '@angular/core';
import { FoldersService } from 'src/app/services/folders.service';
import { AuthService } from 'src/app/services/auth.service';
import { PublicationsService } from 'src/app/services/publications.service';
import { PublicationI } from 'src/app/interfaces/publication';
import { Router } from '@angular/router';

@Component({
	selector: 'app-folders',
	templateUrl: './folders.page.html',
	styleUrls: ['./folders.page.scss'],
})
export class FoldersPage implements OnInit {

	public folders: any[];
	public idusuario: string;
	public foldersUser: Array<{}> = [];
	public publicacion: PublicationI;
	public urlImagen: string;
	public url: boolean = false;
	public repetir: boolean = true;

	constructor(private folderSvc: FoldersService, private authSvc: AuthService, 
			private publicationSvc: PublicationsService, private router: Router) {
		this.idusuario = this.authSvc.getCurrentID();
		this.folderSvc.getFolders().subscribe(carpetas => {
			this.folders = carpetas;
			this.foldersUser = [];

			for (let folder of this.folders) {
				this.publicationSvc.getPublication(folder.publicaciones[0]).subscribe(publication => {
					this.publicacion = publication as PublicationI;
					this.urlImagen = this.publicacion.imagen;
					if (folder.idusuario == this.idusuario) {
						Object.defineProperty(folder, 'checked', {
							value: false,
							writable: true
						});
						Object.defineProperty(folder, 'imagen', {
							value: this.urlImagen,
							writable: true
						});
						this.foldersUser.push(folder);
					}
				})
			}
		});
	}

	ngOnInit() {
	}

}
