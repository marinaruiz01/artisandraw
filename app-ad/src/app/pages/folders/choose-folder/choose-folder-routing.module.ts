import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseFolderPage } from './choose-folder.page';

@NgModule({
  imports: [],
  exports: [RouterModule],
})
export class ChooseFolderPageRoutingModule {}
