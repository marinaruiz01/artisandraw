import { Component, OnInit, Input } from '@angular/core';
import { FoldersService } from 'src/app/services/folders.service';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-choose-folder',
	templateUrl: './choose-folder.page.html',
	styleUrls: ['./choose-folder.page.scss'],
})
export class ChooseFolderPage implements OnInit {
	public folders: any[];
	public foldersUser: Array<{}> = [];
	
	@Input() idusuario;
	@Input() idpublicacion;

	public listaCarpetas: Array<string> = [];

	constructor(private folderSvc: FoldersService, private modalCtrl: ModalController) {
		this.folderSvc.getFolders().subscribe(carpetas => {
			this.folders = carpetas;

			for (let folder of this.folders) {
				if (folder.idusuario == this.idusuario) {
					Object.defineProperty (folder, 'checked', {
						value: false,
						writable: true
					});
					this.foldersUser.push(folder);
				}
			}
		});
	}

	ngOnInit() {
	
	}

	cerrarModal() {

		for (let folder of this.folders) {
			if (folder.checked) {
				this.listaCarpetas.push(folder.id);
			}
		}

		this.modalCtrl.dismiss({
			carpetas: this.listaCarpetas
		});

		this.folderSvc.archive(this.idpublicacion, this.listaCarpetas);
	}

}
