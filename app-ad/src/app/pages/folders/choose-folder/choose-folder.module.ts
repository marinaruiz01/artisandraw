import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseFolderPageRoutingModule } from './choose-folder-routing.module';

import { ChooseFolderPage } from './choose-folder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseFolderPageRoutingModule
  ],
  declarations: [ChooseFolderPage]
})
export class ChooseFolderPageModule {}
