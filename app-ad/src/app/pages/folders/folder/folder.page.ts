import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FoldersService } from 'src/app/services/folders.service';
import { FolderI } from 'src/app/interfaces/folder';
import { PublicationsService } from 'src/app/services/publications.service';
import { PublicationI } from 'src/app/interfaces/publication';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
	public idcarpeta: string;
	public carpeta: any;
	public idpublicaciones: any[];
	public publicacion: any;
	public publicaciones: Array<{}> = [];


  constructor(private route: ActivatedRoute, private folderSvc: FoldersService,
		  private publicationSvc: PublicationsService, private router: Router) { 
	this.idcarpeta = this.route.snapshot.paramMap.get('id');
	this.publicaciones = [];
	
	this.folderSvc.getFolder(this.idcarpeta).subscribe ( carpeta => {
		this.carpeta = carpeta as FolderI;
		this.idpublicaciones = this.carpeta.publicaciones;

		for (let idpublicacion of this.idpublicaciones) {
			this.publicationSvc.getPublication(idpublicacion).subscribe ( publicacion => {
				this.publicacion = publicacion as PublicationI;
				const datos = {
					idpublicacion: this.publicacion.id,
					imagen: this.publicacion.imagen
				}
				this.publicaciones.push(datos);
			});
		}

	});
  }

  ngOnInit() {
  }

  quitarPublicacion(idpublicacion: string) {
	var opcion = confirm("¿Estás seguro de que quieres quitar esta publicación de la carpeta?");

	if (opcion) {
		this.folderSvc.unarchive(idpublicacion, this.idcarpeta);
		this.router.navigate(['/folders']);
	} 

  }

  borrar(id: string) {
	  this.folderSvc.deleteFolder(this.idcarpeta);
  }

}
