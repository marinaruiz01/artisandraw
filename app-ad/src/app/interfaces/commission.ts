export interface CommissionI {
	id: string,
	tipo: string,
	presupuesto: number,
	fecha: string,
	descripcion: string,
	tamaño: string,
	autor: string,
	usuario: string,
	estado: string,
	idautor: string,
	idusuario: string,
	precio: number,
	envio: number
	direccion: string
}