export interface ProductI {
	id: string,
	titulo: string,
	descripcion: string,
	precio: number,
	gastos: string,
	imagen: string,
	refimagen: string,
	stock: number,
	idusuario: string
}