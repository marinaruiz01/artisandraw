export interface PublicationCategoryI {
	id: string,
	idcategoria: string,
	idpublicacion: string
}