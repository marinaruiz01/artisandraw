export interface MessageI {
	id: string,
	idemisor: string,
	idreceptor: string,
	texto: string,
	asunto: string,
	emisor: string
}