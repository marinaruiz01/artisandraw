export interface FolderI {
	id: string,
	nombre: string,
	idusuario: string,
	publicaciones: Array<String>
}