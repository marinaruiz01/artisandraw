export interface UserI {
	email: string,
	nombre: string,
	usuario: string,
	pass: string,
	comisiones_activas: boolean,
	seguidores: Array<string>,
	siguiendo: Array<string>,
	calificacion_vendedor: number,
	calificacion_comprador: number,
	id: string,
	monedero: number,
	verificada: boolean,
	paypal: string
}
