export interface PublicationI {
	titulo: string,
	descripcion: string,
	id: string,
	imagen: string,
	fecha: string,
	comentarios: Array<string>,
	likes: Array<string>,
	idusuario: string
}