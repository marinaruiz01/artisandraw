import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CategoryI } from '../interfaces/category';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class CategoriesService {

	constructor(private firestore: AngularFirestore) { }

	getCategories() {
		return this.firestore.collection('Categoria').snapshotChanges().pipe(map(categories => {
			return categories.map(a => {
				const data = a.payload.doc.data() as CategoryI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}
}
