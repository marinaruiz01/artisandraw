import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, isEmpty } from 'rxjs/operators';
import { PublicationI } from '../interfaces/publication';
import { PublicationCategoryI } from '../interfaces/publication_category';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class PublicationsService {

	public refStorage: any;
	public fechaActual = new Date();
	public fechaActualStr: string;
	public idPublicacion: string;
	public publCats: any[];
	public publications: any[];
	public likes: any[];

	constructor(private firestorage: AngularFireStorage, private firestore: AngularFirestore, private router: Router) {
		this.refStorage = this.firestorage.storage.ref().child('publications');
		this.fechaActualStr = this.fechaActual.getDate() + "/" + (this.fechaActual.getMonth() + 1) + "/" + this.fechaActual.getFullYear();
	}

	uploadImage(refImage: string, file: File) {
		return new Promise((resolve, reject) => {
			const refFile = this.refStorage.child(refImage);
			refFile.put(file)
				.then(snapshot => {
					console.log("Imagen añadida");
					resolve("Funciona");
				})
				.catch(err => {
					console.log("Error al añadir la imagen");
					reject("No funciona");
				});
		});
	}

	downloadImage(refImage: string) {
		const refFile = this.refStorage.child(refImage);
		const urlImage = refFile.getDownloadURL();
		return urlImage;
	}

	getPublications() {
		return this.firestore.collection('Publicacion').snapshotChanges().pipe(map(publications => {
			return publications.map(a => {
				const data = a.payload.doc.data() as PublicationI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getPublicationsUser(idusuario: string) {
		return this.firestore.collection('Publicacion', ref => ref.where('idusuario', '==', idusuario)).snapshotChanges().pipe(map(publications => {
			return publications.map(a => {
				const data = a.payload.doc.data() as PublicationI;
				data.id = a.payload.doc.id;
				console.log(data);
				return data;
			})
		}));
	}

	getPublUser(id: string) {
		var publicaciones: Array<any> = [];
		return new Promise((resolve, reject) => {
			firebase.firestore().collection("Publicacion").where("idusuario", "==", id)
				.get()
				.then(function (querySnapshot) {
					querySnapshot.forEach(function (doc) {
						// doc.data() is never undefined for query doc snapshots
						const data = doc.data() as PublicationI;
						data.id = doc.id;
						publicaciones.push(data);
					});
					resolve(publicaciones);
				}).catch(err => console.log("error" + err));
		});
	}

	getPublicationCategory() {
		return this.firestore.collection('Publicacion_Categoria').snapshotChanges().pipe(map(publcateg => {
			return publcateg.map(a => {
				const data = a.payload.doc.data() as PublicationCategoryI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	addPublication(titulo: string, imagen: string, categorias: string[], idusuario: string, refimage: string, usuario: string) {
		this.firestore.collection('Publicacion').add({
			fecha: this.fechaActualStr,
			idusuario: idusuario,
			imagen: imagen,
			comentarios: [],
			likes: [],
			titulo: titulo,
			refimage: refimage,
			usuario: usuario
		}).then(publication => {
			this.idPublicacion = publication.id;
			for (let categoria of categorias) {
				this.firestore.collection('Publicacion_Categoria').add({
					idpublicacion: this.idPublicacion,
					idcategoria: categoria
				}).catch(err => console.log("Error al subir publicacion_categoria"));
			}
			alert("Publicación añadida correctamente");
			this.router.navigate(['/publications/' + idusuario]);
		}).catch(err => console.log("Error al añadir la publicación"));
	}

	deletePublication(id: string) {
		this.getPublications().subscribe(publications => {
			this.publications = publications;
			for (let publication of this.publications) {
				if (publication.id == id) {
					var refFile = this.refStorage.child(publication.refimage);
					refFile.delete().then(res => {
						console.log("Imagen borrada correctamente");
						this.firestore.collection('Publicacion').doc(id).delete()
							.catch(err => console.log("Error al borrar la publicación" + err));
					}).catch(err => console.log("Error al borrar la imagen" + err));
				}
			}
			this.router.navigate(['/home']);
		})
		this.getPublicationCategory().subscribe(publcatgs => {
			this.publCats = publcatgs;
			for (let publCat of this.publCats) {
				if (publCat.idpublicacion == id) {
					this.firestore.collection('Publicacion_Categoria').doc(publCat.id).delete()
						.then(res => {
							console.log("Publicacion_Categoria borrada correctamente");
						})
						.catch(err => console.log("Error al borrar la publicacion_categoria" + err));
				}
			}
		});
	}

	updatePublication(id: string, datos: any) {
		this.firestore.collection('Publicacion').doc(id).update(datos);
	}

	getPublication(id: string) {
		return this.firestore.collection('Publicacion').doc(id).valueChanges();
	}

	// Registramos el like en la coleccion 'Like'
	setLike(idpublicacion: string, idusuario: string) {

		const likesArray = firebase.firestore.FieldValue.arrayUnion(idusuario);

		this.firestore.collection("Publicacion").doc(idpublicacion).update({ likes: likesArray });

	}

	// Comprobamos si el usuario ya ha dado like anteriormente a la publicacion
	likeExists(idpublicacion: string, idusuario: string) {

		return new Promise(resolve => {
			firebase.firestore().collection("Publicacion").where("id", "==", idpublicacion)
				.where("likes", "array-contains", idusuario)
				.get()
				.then(queryResult => {
					if (queryResult.empty) {
						resolve(false);
					} else {
						resolve(true);
					}
				});
		});

	}

	// Borramos un like
	deleteLike(idpublicacion: string, idusuario: string) {

		const likesArray = firebase.firestore.FieldValue.arrayRemove(idusuario);

		this.firestore.collection("Publicacion").doc(idpublicacion).update({ likes: likesArray });

	}

	newComment(texto: string, usuario: string, idpublicacion: string) {

		const arrayComentarios = firebase.firestore.FieldValue.arrayUnion({
			texto: texto,
			usuario: usuario,
			fecha: this.fechaActualStr
		});
		this.firestore.collection("Publicacion").doc(idpublicacion).update({ comentarios: arrayComentarios });

	}

	deleteComment(texto: string, usuario: string, fecha: string, idpublicacion: string) {

		const borrarComentario = firebase.firestore.FieldValue.arrayRemove({
			texto: texto,
			usuario: usuario,
			fecha: fecha
		});
		this.firestore.collection("Publicacion").doc(idpublicacion).update({ comentarios: borrarComentario });

	}

}
