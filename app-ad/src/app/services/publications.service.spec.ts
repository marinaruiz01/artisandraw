import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { PublicationsService } from './publications.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { RouterModule } from '@angular/router';

describe('PublicationsService', () => {

	let service: PublicationsService;

	beforeEach(() => TestBed.configureTestingModule({
		imports: [
			FormsModule,
			AngularFireModule.initializeApp(firebaseConfig),
			AngularFireAuthModule,
			AngularFirestoreModule,
			AngularFireStorageModule,
			RouterModule.forRoot([])
		]
	})
	);

	it('should be created', () => {
		service = TestBed.get(PublicationsService);
		expect(service).toBeTruthy();
	});

	it('should get publications',
		inject([PublicationsService], async (servicio: PublicationsService) => {
			let result = servicio.getPublicationsUser('7PufweerhEXTzKtzLHWQCEuWQAJ2')
				.subscribe(publicaciones => {
					let publications = publicaciones;
					expect(publications).not.toBeNull();
					expect(publications.length).toBeGreaterThan(0);
				});
			expect(result).not.toBeNull();
		})
	);
});
