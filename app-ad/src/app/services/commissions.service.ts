import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CommissionI } from '../interfaces/commission';
import { map } from 'rxjs/operators';
import { UsersService } from './users.service';

@Injectable({
	providedIn: 'root'
})
export class CommissionsService {

	public fechaActual = new Date();
	public fechaActualStr: string;
	public usuario: any;
	public username: string;

	constructor(private firestore: AngularFirestore, private usersSvc: UsersService) { 
		this.fechaActualStr = this.fechaActual.getDate() + "/" + (this.fechaActual.getMonth() + 1) + "/" + this.fechaActual.getFullYear();
	}

	newCommission(type: string, size: string, description: string, budget: number, user: string, iduser: string, idauthor: string, author: string, price: number, envio: number, direccion: string, estado: string) {
		this.firestore.collection('Pedido').add( {
			tipo: type,
			tamaño: size,
			descripcion: description,
			presupuesto: budget,
			usuario: user,
			idusuario: iduser,
			idautor: idauthor,
			autor: author,
			direccion: direccion,
			envio: envio,
			precio: price,
			estado: estado,
			fecha: this.fechaActualStr
		}).then ( () => alert("Pedido realizado correctamente. Su pedido está en estado '"+estado+"'. Para ver como va su pedido, accede a 'Pedidos realizados'."));
	}

	getCommissionsAuthor(idusuario: string) {
		return this.firestore.collection('Pedido', ref => ref.where('idautor', '==', idusuario)).snapshotChanges().pipe(map(orders => {
			return orders.map(a => {
				const data = a.payload.doc.data() as CommissionI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getCommissionsUser(idusuario: string) {
		return this.firestore.collection('Pedido', ref => ref.where('idusuario', '==', idusuario)).snapshotChanges().pipe(map(orders => {
			return orders.map(a => {
				const data = a.payload.doc.data() as CommissionI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getCommission(id: string) {
		return this.firestore.collection('Pedido').doc(id).valueChanges();
	}

	updateCommission(id:string, datos: any) {
		this.firestore.collection('Pedido').doc(id).update(datos);
	}

	deleteCommission(id: string) {
		this.firestore.collection('Pedido').doc(id).delete();
	}

}
