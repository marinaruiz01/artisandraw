import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import * as CryptoJS from "crypto-js";
import * as firebase from 'firebase/app';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	public encPassword: string = "asdfghjklñ"; // Clave de encriptación predefinida

	constructor(private auth_fire: AngularFireAuth, private router: Router, private firestore: AngularFirestore) { 
	}

	login(email: string, pass: string) {
		return new Promise((resolve, rejected) => {
			this.auth_fire.auth.signInWithEmailAndPassword(email, pass)
				.then(user => resolve(user))
				.catch(err => rejected(err));
		});
	}

	logout() {
		this.auth_fire.auth.signOut();
		this.router.navigate(['/login']);
	}

	register(email: string, pass: string, name: string, username: string) {
		return new Promise((resolve, rejected) => {
			this.auth_fire.auth.createUserWithEmailAndPassword(email, pass)
				.then(user => {
					pass = CryptoJS.AES.encrypt(pass.trim(), this.encPassword.trim()).toString();
					this.firestore.collection('Usuario').doc(user.user.uid).set({
						email: email,
						pass: pass,
						nombre: name,
						usuario: username,
						seguidores: [],
						siguiendo: [],
						comisiones_activas: false,
						paypal: "",
						monedero: 0,
						direccion: "",
						verificada: false
					});
					resolve(user);
				})
				.catch(err => rejected(err));
		});
	}

	getCurrentID() {
		return this.auth_fire.auth.currentUser.uid;
	}

	deleteUser(pass: string) {
		var user = this.auth_fire.auth.currentUser;
		var credentials = firebase.auth.EmailAuthProvider.credential(
			user.email,
			pass
		);
		user.reauthenticateWithCredential(credentials).then( () => {
			user.delete().then( () => alert ("Tu cuenta ha sido borrada correctamente."));
		}).catch( err => console.log("error credential: "+err));
	}

	editPass (pass: string, newpass: string) {
		var user = this.auth_fire.auth.currentUser;
		var credentials = firebase.auth.EmailAuthProvider.credential (
			user.email,
			pass
		);
		user.reauthenticateWithCredential(credentials).then( () => {
			user.updatePassword(newpass).then ( () => alert ("Contraseña modificada correctamente,"));
		})
	}
}
