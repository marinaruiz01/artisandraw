import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { stringify } from 'querystring';
import { MessageI } from '../interfaces/message';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class MessagesService {

	constructor(private firestore: AngularFirestore) {

	}

	newMessage(idemisor: string, emisor: string, idreceptor: string, asunto: string, texto: string, respondido: string) {
		return new Promise ((resolve, reject) => {
			this.firestore.collection('Mensaje').add({
				idemisor: idemisor,
				idreceptor: idreceptor,
				asunto: asunto,
				emisor: emisor,
				texto: texto,
				idrespondido: respondido
			}).then(() => {
				alert("Mensaje enviado correctamente.");
				resolve(true);
			}).catch(() => {
				reject(false);
			});
		});
	}

	getMessagesUser(iduser: string) {
		return this.firestore.collection('Mensaje', ref => ref.where('idreceptor', '==', iduser)).snapshotChanges().pipe(map(mensajes => {
			return mensajes.map(a => {
				const data = a.payload.doc.data() as MessageI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getMessage(id: string) {
		return this.firestore.collection('Mensaje').doc(id).valueChanges();
	}

	deleteMessage(id: string) {
		this.firestore.collection('Mensaje').doc(id).delete();
	}

}
