import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { FolderI } from '../interfaces/folder';
import * as firebase from 'firebase/app';

@Injectable({
	providedIn: 'root'
})
export class FoldersService {

	public carpetas: any[];

	constructor(private firestore: AngularFirestore) { }

	createFolder(nombre: string, idusuario: string) {
		this.firestore.collection("Carpeta").add({
			nombre: nombre,
			idusuario: idusuario
		});
	}

	getFolders() {

		return this.firestore.collection('Carpeta').snapshotChanges().pipe(map(folders => {
			return folders.map(a => {
				const data = a.payload.doc.data() as FolderI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));

	}

	getFolder(idcarpeta: string) {
		
		return this.firestore.collection('Carpeta').doc(idcarpeta).valueChanges();
	
	}

	archive(idpublicacion: string, carpetas: Array<string>) {

		for (let carpeta of carpetas) {

			const publicationArray = firebase.firestore.FieldValue.arrayUnion(idpublicacion);
			this.firestore.collection("Carpeta").doc(carpeta).update({ publicaciones: publicationArray });

		}

	}

	unarchive (idpublicacion: string, idcarpeta: string) {
		const publicationArray = firebase.firestore.FieldValue.arrayRemove(idpublicacion);
		this.firestore.collection("Carpeta").doc(idcarpeta).update({ publicaciones: publicationArray });
	}

	cleanFolders (idpublicacion: string) {
		this.getFolders().subscribe(folders => {
			this.carpetas = folders;
			for (let carpeta of this.carpetas) {
				this.unarchive(idpublicacion, carpeta.id);
			} 
		})
	}

	deleteFolder (id: string) {
		this.firestore.collection('Carpeta').doc(id).delete().then ( () => {
			alert ("Carpeta borrada correctamente");
		})
	}

}
