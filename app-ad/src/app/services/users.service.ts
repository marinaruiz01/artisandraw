import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { UserI } from "../interfaces/user";
import { AuthService } from "./auth.service";
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Injectable({
	providedIn: 'root'
})
export class UsersService {

	public user_id: string;
	//public user: UserI;
	//public refStorage: any;
	public usuarioActual: any;
	public usuarioBorrar: any;
	public encPassword: string = "asdfghjklñ";

	constructor(private firestore: AngularFirestore, 
			private auth_service: AuthService) {
	}

	getCurrentUser() {
		this.user_id = this.auth_service.getCurrentID();
		return this.firestore.collection('Usuario').doc(this.user_id).valueChanges();
	}

	getUserActual() {
		this.user_id = this.auth_service.getCurrentID();
		return new Promise(resolve => {
			this.firestore.collection('Usuario').doc(this.user_id).valueChanges().subscribe(user => {
				var usuario: any = user;
				resolve(usuario);
			});
		});
	}

	editCurrentUser(datos: any) {
		this.user_id = this.auth_service.getCurrentID();
		this.firestore.collection('Usuario').doc(this.user_id).update(datos);
	}

	/*uploadAvatar(refImage: string, file: File) {
		const refFile = this.refStorage.child(refImage);
		refFile.put(file).then(snapshot => {
			alert("Se ha modificado la foto de perfil correctamente");
		});
	}

	downloadAvatar(refImage: string) {
		const refFile = this.refStorage.child(refImage);
		const urlImage = refFile.getDownloadURL();
		return urlImage;
	}*/

	getUsers() {

		return this.firestore.collection('Usuario').snapshotChanges().pipe(map(users => {
			return users.map(a => {
				const data = a.payload.doc.data() as UserI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getUser(id: string) {
		return this.firestore.collection('Usuario').doc(id).valueChanges();
	}

	editUser(id: string, datos: any) {
		this.firestore.collection('Usuario').doc(id).update(datos);
	}

	followUser(idusuario: string) {
		this.user_id = this.auth_service.getCurrentID();

		const followArray = firebase.firestore.FieldValue.arrayUnion(this.user_id);
		const followingArray = firebase.firestore.FieldValue.arrayUnion(idusuario);

		this.firestore.collection("Usuario").doc(idusuario).update({ seguidores: followArray }).then(res => {
			this.firestore.collection("Usuario").doc(this.user_id).update({ siguiendo: followingArray });
		});
	}

	unfollowUser(idusuario: string) {
		this.user_id = this.auth_service.getCurrentID();

		const followArray = firebase.firestore.FieldValue.arrayRemove(this.user_id);
		const followingArray = firebase.firestore.FieldValue.arrayRemove(idusuario);

		this.firestore.collection("Usuario").doc(idusuario).update({ seguidores: followArray }).then(res => {
			this.firestore.collection("Usuario").doc(this.user_id).update({ siguiendo: followingArray });
		});
	}

	followExists(idusuario: string) {
		this.user_id = this.auth_service.getCurrentID();

		return new Promise(resolve => {
			firebase.firestore().collection("Usuario").where("id", "==", this.user_id)
				.where("siguiendo", "array-contains", idusuario)
				.get()
				.then(queryResult => {
					if (queryResult.empty) {
						resolve(false);
					} else {
						resolve(true);
					}
				});
		});
	}

	deleteUser(id: string) {
		this.firestore.collection('Usuario').doc(id).delete();
	}

	cleanFollows(idborrado: string, iduser: string) {
		const followArray = firebase.firestore.FieldValue.arrayRemove(idborrado);
		const followingArray = firebase.firestore.FieldValue.arrayRemove(idborrado);

		this.firestore.collection("Usuario").doc(iduser).update({ seguidores: followArray }).then(res => {
			this.firestore.collection("Usuario").doc(iduser).update({ siguiendo: followingArray });
		});
	}

}
