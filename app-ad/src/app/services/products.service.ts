import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { ProductI } from '../interfaces/product';

@Injectable({
	providedIn: 'root'
})
export class ProductsService {

	public refStorage: any;

	constructor(private firestorage: AngularFireStorage, private firestore: AngularFirestore) {
		this.refStorage = this.firestorage.storage.ref().child('products');
	}

	uploadImage(refImage: string, file: File) {
		return new Promise((resolve, reject) => {
			const refFile = this.refStorage.child(refImage);
			refFile.put(file)
				.then(snapshot => {
					console.log("Imagen añadida");
					resolve("Funciona");
				})
				.catch(err => {
					console.log("Error al añadir la imagen");
					reject("No funciona");
				});
		});
	}

	downloadImage (refImage: string) {
		const refFile = this.refStorage.child(refImage);
		const urlImage = refFile.getDownloadURL();
		return urlImage;
	}

	addProduct (titulo: string, descripcion: string, precio: number, gastos: number,
			stock: number, imagen: string, idusuario: string, refimage: string) {
		this.firestore.collection('Producto').add({
			idusuario: idusuario,
			imagen: imagen,
			refimagen: refimage,
			titulo: titulo,
			descripcion: descripcion,
			precio: precio,
			gastos: gastos,
			stock: stock
		}).then ( res => alert ("Producto añadido correctamente"))
		.catch ( err => console.log ("Error al añadir el producto"));

	}

	getProducts () {
		return this.firestore.collection('Producto').snapshotChanges().pipe(map(products => {
			return products.map(a => {
				const data = a.payload.doc.data() as ProductI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getProductsUser(iduser: string) {
		return this.firestore.collection('Producto', ref => ref.where('idusuario', '==', iduser)).snapshotChanges().pipe(map(products => {
			return products.map(a => {
				const data = a.payload.doc.data() as ProductI;
				data.id = a.payload.doc.id;
				return data;
			})
		}));
	}

	getProduct (idproducto: string) {
		return this.firestore.collection('Producto').doc(idproducto).valueChanges();
	}

	deleteProduct (id: string) {
		this.firestore.collection('Producto').doc(id).delete();
	}
}
