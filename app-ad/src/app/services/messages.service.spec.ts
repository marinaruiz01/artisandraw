import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { MessagesService } from './messages.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { firebaseConfig } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { RouterModule } from '@angular/router';

describe('MessagesService', () => {

	let service: MessagesService;
	let result;

	beforeEach(() => TestBed.configureTestingModule({
		imports: [
			FormsModule,
			AngularFireModule.initializeApp(firebaseConfig),
			AngularFireAuthModule,
			AngularFirestoreModule,
			AngularFireStorageModule,
			RouterModule.forRoot([])]
	})
	);

	it('should be created', () => {
		service = TestBed.get(MessagesService);
		expect(service).toBeTruthy();
	});

	it('should create message', inject([MessagesService], async (servicio: MessagesService) => {
		result = servicio.newMessage('asdf9as2ldsT5x2', 'usuarioprueba', 'pslEzcn2Bs34',
		'asunto prueba', 'este es el texto del mensaje', '');
		expect (result).not.toBeNull();
		expect (result).toBeTruthy();
	}));
});
