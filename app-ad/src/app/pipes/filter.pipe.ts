import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'filter'
})
export class FilterPipe implements PipeTransform {

	transform(values: any[], text: string, column: string): any {
		
		if (text === '') {
			return values;
		} 

		text = text.toLowerCase();
		
		return values.filter ( item  => {
			return item[column].toLowerCase().includes(text);
		});
	}
}
